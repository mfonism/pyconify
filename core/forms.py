from django.core.exceptions import ValidationError
from django.forms import ModelForm, Form, CharField

from .models import Talk


class TalkCreationForm(ModelForm):
    class Meta:
        model = Talk
        fields = ['author_name', 'title', 'abstract', 'category', 'level', 'additional_notes']

    def __init__(self, *args, **kwargs):
        self.request = kwargs.pop('request')
        self.conference_pk = kwargs.pop('conference_pk')
        super().__init__(*args, **kwargs)

    def save(self, commit=True):
        instance = super().save(commit=False)
        instance.conference_id = self.conference_pk
        instance.proposer = self.request.user
        if commit:
            instance.save()
        return instance


class TalkUpdateForm(ModelForm):
    class Meta:
        model = Talk
        fields = ['author_name', 'title', 'abstract', 'category', 'level', 'additional_notes']


class TalkDeletionForm(Form):
    confirmation_word = CharField()

    def __init__(self, *args, **kwargs):
        self.request = kwargs.pop('request')
        super().__init__(*args, **kwargs)

    def clean_confirmation_word(self):
        data = self.cleaned_data['confirmation_word']
        if data != self.request.user.email:
            raise ValidationError(
                'Invalid confirmation word: %(confirmation_word)s', code='invalid', params={'confirmation_word': data}
            )
        return data
