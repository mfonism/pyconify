from django.db import models


class CategoryChoices(models.TextChoices):
    COMMUNITY_BUILDING = ('COMMUNITY', 'Community Building')
    DATA_SCIENCE = ('DS', 'Data Science')
    ARTIFICIAL_INTELLIGENCE = ('AI', 'Artificial Intelligence')
    WEB_DEVELOPMENT = ('WEB DEV', 'Web Development')
    DEVOPS = ('DEV OPS', 'Dev Ops')
    EMBEDDED_SYSTEMS = ('EMBEDDED', 'Embedded Systems')
    OTHER = ('OTHER', 'Other')


class LevelChoices(models.IntegerChoices):
    ALL = (0, 'Everyone')
    BEGINNER = (1, 'Beginner')
    INTERMEDIATE = (2, 'Intermediate')
    EXPERT = (3, 'Expert')
