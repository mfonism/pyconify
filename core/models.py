from django.conf import settings
from django.db import models
from django.urls import reverse

from .choices import CategoryChoices, LevelChoices


class Conference(models.Model):
    year = models.IntegerField(primary_key=True)
    basename = models.CharField(max_length=64)

    def __str__(self):
        return self.get_fullname()

    def get_fullname(self):
        return f'{self.basename} {self.year}'


class Talk(models.Model):
    conference = models.ForeignKey(Conference, on_delete=models.CASCADE)
    proposer = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    author_name = models.CharField(max_length=64)
    title = models.CharField(max_length=128)
    abstract = models.TextField()
    category = models.CharField(max_length=32, choices=CategoryChoices.choices)
    level = models.IntegerField(choices=LevelChoices.choices)
    additional_notes = models.TextField(blank=True)

    def get_absolute_url(self):
        return reverse('talk_read', kwargs={'conference_pk': self.conference.year, 'talk_pk': self.pk})

    def get_additional_notes(self):
        return self.additional_notes or '-'
