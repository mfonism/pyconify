from django.contrib import admin

from .models import Conference, Talk


@admin.register(Conference)
class ConferenceAdmin(admin.ModelAdmin):
    pass


@admin.register(Talk)
class TalkAdmin(admin.ModelAdmin):
    pass
