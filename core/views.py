from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib import messages
from django.shortcuts import get_object_or_404
from django.views.generic import CreateView, DetailView, FormView, TemplateView, UpdateView
from django.views.generic.edit import SingleObjectMixin
from django.urls import reverse

from .forms import TalkCreationForm, TalkDeletionForm, TalkUpdateForm
from .models import Conference, Talk


class HomeView(DetailView):
    model = Conference
    template_name = 'core/home.html'
    pk_url_kwarg = 'conference_pk'


class AboutView(DetailView):
    model = Conference
    template_name = 'core/about.html'
    pk_url_kwarg = 'conference_pk'


class SponsorsView(DetailView):
    model = Conference
    template_name = 'core/sponsors.html'
    pk_url_kwarg = 'conference_pk'


class ConductView(DetailView):
    model = Conference
    template_name = 'core/conduct.html'
    pk_url_kwarg = 'conference_pk'


class TalkCreateView(LoginRequiredMixin, CreateView):
    model = Talk
    form_class = TalkCreationForm
    template_name = 'core/talk_create.html'

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs['conference_pk'] = self.kwargs['conference_pk']
        kwargs['request'] = self.request
        return kwargs

    def get_context_data(self, **kwargs):
        context_data = super().get_context_data(**kwargs)
        conference = get_object_or_404(Conference, pk=self.kwargs['conference_pk'])
        context_data['conference'] = conference
        return context_data


class TalkReadView(LoginRequiredMixin, DetailView):
    pk_url_kwarg = 'talk_pk'
    template_name = 'core/talk_read.html'

    def get_queryset(self):
        return Talk.objects.filter(proposer_id=self.request.user.pk, conference_id=self.kwargs['conference_pk'])

    def get_context_data(self, **kwargs):
        context_data = super().get_context_data(**kwargs)
        conference = get_object_or_404(Conference, pk=self.kwargs['conference_pk'])
        context_data['conference'] = conference
        return context_data


class TalkUpdateView(LoginRequiredMixin, UpdateView):
    pk_url_kwarg = 'talk_pk'
    form_class = TalkUpdateForm
    template_name = 'core/talk_update.html'

    def get_queryset(self):
        return Talk.objects.filter(proposer_id=self.request.user.pk, conference_id=self.kwargs['conference_pk'])

    def get_context_data(self, **kwargs):
        context_data = super().get_context_data(**kwargs)
        conference = get_object_or_404(Conference, pk=self.kwargs['conference_pk'])
        context_data['conference'] = conference
        return context_data


class TalkDeleteView(LoginRequiredMixin, SingleObjectMixin, FormView):
    pk_url_kwarg = 'talk_pk'
    template_name = 'core/talk_delete.html'
    form_class = TalkDeletionForm

    def get(self, request, *args, **kwargs):
        self.object = self.get_object()
        return super().get(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        self.object = self.get_object()
        return super().post(request, *args, **kwargs)

    def get_queryset(self):
        return Talk.objects.filter(proposer_id=self.request.user.pk, conference_id=self.kwargs['conference_pk'])

    def get_success_url(self):
        return reverse('talk_delete_success', kwargs={'conference_pk': self.kwargs['conference_pk']})

    def form_valid(self, form):
        self.object.delete()
        messages.info(self.request, message=self.object.title, extra_tags='talk_title')
        return super().form_valid(form)

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs['request'] = self.request
        return kwargs

    def get_context_data(self, **kwargs):
        context_data = super().get_context_data(**kwargs)
        conference = get_object_or_404(Conference, pk=self.kwargs['conference_pk'])
        context_data['conference'] = conference
        return context_data


class TalkDeleteSuccessView(LoginRequiredMixin, TemplateView):
    template_name = 'core/talk_delete_success.html'

    def get_context_data(self, **kwargs):
        context_data = super().get_context_data(**kwargs)
        conference = get_object_or_404(Conference, pk=self.kwargs['conference_pk'])
        context_data['conference'] = conference
        return context_data
