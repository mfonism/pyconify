# Generated by Django 3.1.6 on 2021-02-19 19:59

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0011_talk_additional_notes'),
    ]

    operations = [
        migrations.AlterField(
            model_name='talk',
            name='additional_notes',
            field=models.TextField(blank=True, null=True),
        ),
    ]
