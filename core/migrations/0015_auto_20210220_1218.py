# Generated by Django 3.1.6 on 2021-02-20 11:18

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0014_talk_proposer'),
    ]

    operations = [
        migrations.AlterField(
            model_name='talk',
            name='additional_notes',
            field=models.TextField(blank=True, default=''),
            preserve_default=False,
        ),
    ]
