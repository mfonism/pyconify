# Generated by Django 3.1.6 on 2021-02-19 19:39

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0010_talk_level'),
    ]

    operations = [
        migrations.AddField(
            model_name='talk',
            name='additional_notes',
            field=models.TextField(default='yes additional notes'),
            preserve_default=False,
        ),
    ]
