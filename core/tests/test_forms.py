from dataclasses import dataclass

from django.contrib.auth import get_user_model
from django.test import TestCase

from core.choices import CategoryChoices, LevelChoices
from core.forms import TalkCreationForm, TalkDeletionForm


@dataclass
class MockRequest:
    user: get_user_model()


class TestTalkCreationForm(TestCase):
    @classmethod
    def setUpTestData(cls):
        cls.user = get_user_model().objects.create_user(
            username='proposer007', email='proposer007@aol.com', password='proposer007'
        )
        cls.request = MockRequest(user=cls.user)
        cls.valid_data = {
            'author_name': 'Mfon Eti-mfon',
            'title': 'Yet Another Python Talk',
            'abstract': 'Have you ever wondered about...',
            'category': CategoryChoices.COMMUNITY_BUILDING,
            'level': LevelChoices.ALL,
            'additional_notes': '',
        }

    def test_save(self):
        conference_year = 2021
        form = TalkCreationForm(data=self.valid_data, request=self.request, conference_pk=conference_year)

        talk = form.save(commit=False)

        self.assertEqual(talk.conference_id, conference_year)
        self.assertEqual(talk.proposer, self.user)


class TestTalkDeletionForm(TestCase):
    @classmethod
    def setUpTestData(cls):
        cls.user = get_user_model().objects.create_user(
            username='proposer007', email='proposer007@aol.com', password='proposer007'
        )
        cls.request = MockRequest(user=cls.user)

    def test_clean_confirmation_word(self):
        form = TalkDeletionForm(data={'confirmation_word': self.user.email}, request=self.request)

        self.assertTrue(form.is_valid())
        self.assertEqual(form.cleaned_data['confirmation_word'], self.user.email)

    def test_clean_confirmation_word_error(self):
        wrong_word = 'wrongword@aol.com'
        form = TalkDeletionForm({'confirmation_word': wrong_word}, request=self.request)

        self.assertFalse(form.is_valid())
        confirmation_word_errors = form.errors['confirmation_word']
        expected_error = f'Invalid confirmation word: {wrong_word}'
        self.assertIn(expected_error, confirmation_word_errors)
