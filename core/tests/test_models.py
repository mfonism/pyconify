from django.contrib.auth import get_user_model
from django.test import TestCase

from core.choices import CategoryChoices, LevelChoices
from core.models import Conference, Talk


class TestConference(TestCase):
    @classmethod
    def setUpTestData(cls):
        cls.conf = Conference.objects.create(basename='PyCon NG', year=2021)

    def test_get_conference_fullname(self):
        self.assertEqual(self.conf.get_fullname(), 'PyCon NG 2021')

    def test_str_conference(self):
        self.assertEqual(str(self.conf), 'PyCon NG 2021')


class TestTalk(TestCase):
    @classmethod
    def setUpTestData(cls):
        cls.conf = Conference.objects.create(basename='PyCon NG', year=2021)
        cls.proposer = get_user_model().objects.create_user(
            username='proposer007', email='proposer007@aol.com', password='hydrogen bond'
        )

    def test_get_additional_notes(self):
        additional_notes = 'I\'d love to have a VGA to HDMI adapter...'
        talk = Talk.objects.create(
            conference=self.conf,
            proposer=self.proposer,
            author_name='Mfon Eti-mfon',
            title='Yet Another Python Talk',
            abstract='Have you ever...',
            category=CategoryChoices.DEVOPS,
            level=LevelChoices.INTERMEDIATE,
            additional_notes=additional_notes,
        )

        self.assertNotEqual(talk.additional_notes, '')
        self.assertEqual(talk.get_additional_notes(), additional_notes)

    def test_get_additional_notes_for_talk_with_no_additional_notes(self):
        talk = Talk.objects.create(
            conference=self.conf,
            proposer=self.proposer,
            author_name='Mfon Eti-mfon',
            title='Yet Another Python Talk',
            abstract='Have you ever...',
            category=CategoryChoices.DEVOPS,
            level=LevelChoices.INTERMEDIATE,
        )

        self.assertEqual(talk.additional_notes, '')
        self.assertEqual(talk.get_additional_notes(), '-')
