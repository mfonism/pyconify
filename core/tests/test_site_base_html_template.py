from django.template.loader import render_to_string
from django.test import TestCase
from django.urls import reverse

from core.models import Conference


class TestSiteBaseHTMLTemplate(TestCase):
    def test_content(self):
        conf = Conference.objects.create(basename='PyCon NG', year=2021)
        rendered_content = render_to_string('site_base.html', {'conference': conf})

        self.assertIn(conf.get_fullname(), rendered_content)

        self.assertIn('Home', rendered_content)
        home_url = reverse('home', kwargs={'conference_pk': conf.year})
        self.assertIn(home_url, rendered_content)

        self.assertIn('About', rendered_content)
        about_url = reverse('about', kwargs={'conference_pk': conf.year})
        self.assertIn(about_url, rendered_content)

        self.assertIn('Sponsors', rendered_content)
        sponsors_url = reverse('sponsors', kwargs={'conference_pk': conf.year})
        self.assertIn(sponsors_url, rendered_content)

        self.assertIn('Speaking', rendered_content)
        self.assertIn('Propose a Talk', rendered_content)
        talk_create_url = reverse('talk_create', kwargs={'conference_pk': conf.year})
        self.assertIn(talk_create_url, rendered_content)

        self.assertIn('Conduct', rendered_content)
        conduct_url = reverse('conduct', kwargs={'conference_pk': conf.year})
        self.assertIn(conduct_url, rendered_content)
