from django.test import TestCase
from django.urls import reverse

from core.models import Conference


class TestAboutView(TestCase):
    @classmethod
    def setUpTestData(cls):
        cls.conf = Conference.objects.create(basename='PyCon NG', year=2021)
        cls.url = reverse('about', kwargs={'conference_pk': cls.conf.year})

    def test_get_about_page(self):
        resp = self.client.get(self.url)

        self.assertEqual(resp.status_code, 200)
        self.assertTemplateUsed(resp, 'core/about.html')
        self.assertContains(resp, f'About { self.conf.basename }')
        self.assertTemplateUsed(resp, 'site_base.html')
