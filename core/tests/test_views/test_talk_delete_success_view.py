from django.contrib.auth import get_user_model
from django.test import TestCase
from django.urls import reverse

from core.models import Conference


class TestTalkDeleteSuccessView(TestCase):
    @classmethod
    def setUpTestData(cls):
        cls.conf = Conference.objects.create(basename='PyCon NG', year=2021)
        cls.proposer = get_user_model().objects.create_user(
            username='proposer007', password='proposer007', email='proposer007@aol.com'
        )
        cls.url = reverse('talk_delete_success', kwargs={'conference_pk': cls.conf.year})

    def test_unauthenticated_user_cannot_get_talk_delete_success_page(self):
        resp = self.client.get(self.url)

        self.assertEqual(resp.status_code, 302)

    def test_authenticated_user_can_get_talk_delete_success_page(self):
        self.client.force_login(self.proposer)
        resp = self.client.get(self.url)

        self.assertEqual(resp.status_code, 200)
        self.assertTemplateUsed(resp, 'core/talk_delete_success.html')
        self.assertTemplateUsed(resp, 'site_base.html')
        self.assertContains(resp, 'Run along, there\'s nothing to see here!')
