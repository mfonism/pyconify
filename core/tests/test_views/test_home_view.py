from django.test import TestCase
from django.urls import reverse

from core.models import Conference


class TestHomeView(TestCase):
    @classmethod
    def setUpTestData(cls):
        cls.conf = Conference.objects.create(basename='PyCon NG', year=2021)
        cls.url = reverse('home', kwargs={'conference_pk': cls.conf.year})

    def test_get_home_page(self):
        resp = self.client.get(self.url)

        self.assertEqual(resp.status_code, 200)
        self.assertTemplateUsed(resp, 'core/home.html')
        self.assertContains(resp, self.conf.get_fullname())
        self.assertTemplateUsed(resp, 'site_base.html')
