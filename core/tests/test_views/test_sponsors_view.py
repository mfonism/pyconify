from django.test import TestCase
from django.urls import reverse

from core.models import Conference


class TestSponsorsView(TestCase):
    @classmethod
    def setUpTestData(cls):
        cls.conf = Conference.objects.create(basename='PyCon NG', year=2021)
        cls.url = reverse('sponsors', kwargs={'conference_pk': cls.conf.year})

    def test_get_sponsors_page(self):
        resp = self.client.get(self.url)

        self.assertEqual(resp.status_code, 200)
        self.assertTemplateUsed(resp, 'core/sponsors.html')
        self.assertContains(resp, f'Sponsor { self.conf.basename }')
        self.assertTemplateUsed(resp, 'site_base.html')
