from django.contrib.auth import get_user_model
from django.contrib.messages import get_messages
from django.test import TestCase
from django.urls import reverse

from core.choices import CategoryChoices, LevelChoices
from core.models import Conference, Talk


class TestTalkDeleteVeiw(TestCase):
    @classmethod
    def setUpTestData(cls):
        cls.conf = Conference.objects.create(basename='PyCon NG', year=2021)
        cls.proposer = get_user_model().objects.create_user(
            username='proposer007', password='proposer007', email='proposer007@aol.com'
        )
        cls.talk = Talk.objects.create(
            conference=cls.conf,
            proposer=cls.proposer,
            author_name='Mfon Eti-mfon',
            title='Yet Another Python Talk',
            abstract='Have you ever...',
            category=CategoryChoices.DEVOPS,
            level=LevelChoices.INTERMEDIATE,
        )
        cls.url = reverse('talk_delete', kwargs={'conference_pk': cls.conf.year, 'talk_pk': cls.talk.pk})
        cls.payload = {'confirmation_word': cls.proposer.email}

    def test_unauthenticated_user_cannot_get_talk_deletion_page(self):
        resp = self.client.get(self.url)

        self.assertEqual(resp.status_code, 302)

    def test_unauthenticated_user_cannot_delete_talk(self):
        resp = self.client.post(self.url, self.payload)

        self.assertEqual(resp.status_code, 302)
        self.assertTrue(Talk.objects.filter(pk=self.talk.pk).exists())

    def test_authenticated_user_can_get_talk_deletion_page(self):
        self.client.force_login(self.proposer)
        resp = self.client.get(self.url)

        self.assertEqual(resp.status_code, 200)
        self.assertTemplateUsed(resp, 'core/talk_delete.html')
        self.assertTemplateUsed(resp, 'site_base.html')
        self.assertContains(resp, self.talk.title)
        self.assertContains(resp, self.proposer.email)
        self.assertContains(resp, 'confirmation_word')
        self.assertContains(resp, 'Confirm Delete')

    def test_authenticated_user_can_delete_talk(self):
        self.client.force_login(self.proposer)

        resp = self.client.post(self.url, self.payload)

        self.assertRedirects(resp, reverse('talk_delete_success', kwargs={'conference_pk': self.conf.year}))
        self.assertFalse(Talk.objects.filter(pk=self.talk.pk).exists())

    def test_message_added_on_talk_deletion(self):
        self.client.force_login(self.proposer)

        resp = self.client.post(self.url, self.payload, follow=True)

        self.assertEqual(resp.status_code, 200)
        # assert that an appropriate message is added to the messages storage
        expected_message = self.talk.title
        message_storage = get_messages(resp.wsgi_request)
        messages = [str(message) for message in message_storage if 'talk_title' in message.tags.split()]
        self.assertIn(expected_message, messages)

        # assert that this message shows up in response body
        self.assertContains(resp, 'You\'ve just deleted your talk titled,')

    def test_authenticated_user_cannot_delete_talk_with_incorrect_confirmation_word(self):
        self.client.force_login(self.proposer)

        resp = self.client.post(self.url, {'confirmation_word': 'incorrect_confirmation_word'})

        self.assertEqual(resp.status_code, 200)
        self.assertTrue(Talk.objects.filter(pk=self.talk.pk).exists())

    def test_authenticated_user_cannot_get_someone_elses_talk_deletion_page(self):
        another_proposer = get_user_model().objects.create_user(
            username='anotherproposer', password='nopassword', email='anotherproposer@aol.com'
        )

        self.client.force_login(another_proposer)
        resp = self.client.get(self.url)

        self.assertEqual(resp.status_code, 404)

    def test_authenticated_user_cannot_delete_someone_elses_talk(self):
        another_proposer = get_user_model().objects.create_user(
            username='anotherproposer', password='nopassword', email='anotherproposer@aol.com'
        )

        self.client.force_login(another_proposer)
        resp = self.client.post(self.url, self.payload)

        self.assertEqual(resp.status_code, 404)

    def test_authenticated_user_cannot_get_talk_deletion_page_with_mismatching_url_kwargs(self):
        url = reverse('talk_delete', kwargs={'conference_pk': self.conf.pk + 1, 'talk_pk': self.talk.pk})

        self.client.force_login(self.proposer)
        resp = self.client.get(url)

        self.assertEqual(resp.status_code, 404)

    def test_authenticated_user_cannot_delete_talk_with_mismatching_url_kwargs(self):
        url = reverse('talk_delete', kwargs={'conference_pk': self.conf.pk + 1, 'talk_pk': self.talk.pk})

        self.client.force_login(self.proposer)
        resp = self.client.post(url, self.payload)

        self.assertEqual(resp.status_code, 404)
