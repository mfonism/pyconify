from django.contrib.auth import get_user_model
from django.forms.models import model_to_dict
from django.test import TestCase
from django.urls import reverse

from core.choices import CategoryChoices, LevelChoices
from core.models import Conference, Talk


class TestTalkUpdateView(TestCase):
    @classmethod
    def setUpTestData(cls):
        cls.proposer = get_user_model().objects.create_user(
            username='proposer007', email='proposer007@aol.com', password='hydrogen bond'
        )
        cls.conf = Conference.objects.create(basename='PyCon NG', year=2021)

    def setUp(self):
        self.talk = Talk.objects.create(
            conference=self.conf,
            proposer=self.proposer,
            author_name='Mfon Eti-mfon',
            title='Yet Another Python Talk',
            abstract='Have you ever...',
            category=CategoryChoices.DEVOPS,
            level=LevelChoices.INTERMEDIATE,
        )
        self.url = reverse('talk_update', kwargs={'conference_pk': self.conf.pk, 'talk_pk': self.talk.pk})

        self.payload = {
            'author_name': 'Mfon Kiwi Eti-mfon',
            'title': self.talk.title,
            'abstract': self.talk.abstract,
            'category': self.talk.category,
            'level': LevelChoices.ALL,
            'additional_notes': 'I\'d like to have TAs...',
        }

    def test_unauthenticated_user_cannot_get_talk_update_page(self):
        resp = self.client.get(self.url)

        self.assertEqual(resp.status_code, 302)

    def test_unauthenticated_user_cannot_update_talk(self):
        resp = self.client.post(self.url, self.payload)

        self.assertEqual(resp.status_code, 302)
        self.talk.refresh_from_db()
        talk_data = model_to_dict(self.talk, fields=self.payload.keys())
        self.assertNotEqual(talk_data, self.payload)

    def test_authenticated_user_can_get_talk_update_page_for_their_talk(self):
        self.client.force_login(self.proposer)
        resp = self.client.get(self.url)

        self.assertEqual(resp.status_code, 200)
        self.assertTemplateUsed(resp, 'core/talk_update.html')
        self.assertTemplateUsed(resp, 'site_base.html')
        self.assertContains(resp, self.talk.author_name)
        self.assertContains(resp, self.talk.title)
        self.assertContains(resp, self.talk.abstract)
        self.assertContains(resp, self.talk.category)
        self.assertContains(resp, self.talk.level)
        self.assertContains(resp, self.talk.additional_notes)
        self.assertContains(resp, 'Update Talk')

    def test_authenticated_user_can_update_their_talk(self):
        self.client.force_login(self.proposer)
        resp = self.client.post(self.url, self.payload)

        self.assertRedirects(resp, self.talk.get_absolute_url())
        self.talk.refresh_from_db()
        talk_data = model_to_dict(self.talk, fields=self.payload.keys())
        self.assertEqual(talk_data, self.payload)

    def test_authenticated_user_cannot_get_talk_update_page_for_someone_elses_talk(self):
        another_proposer = get_user_model().objects.create_user(
            username='anotherproposer', password='anotherproposer', email='anotherproposer@aol.com'
        )
        talk = Talk.objects.create(
            conference=self.conf,
            proposer=another_proposer,
            author_name='Mfon Eti-mfon',
            title='Yet Another Python Talk',
            abstract='Have you ever...',
            category=CategoryChoices.DEVOPS,
            level=LevelChoices.INTERMEDIATE,
        )
        talk_update_url = reverse('talk_update', kwargs={'conference_pk': self.talk.conference.pk, 'talk_pk': talk.pk})

        self.client.force_login(self.proposer)
        resp = self.client.get(talk_update_url)

        self.assertEqual(resp.status_code, 404)

    def test_authenticated_user_cannot_update_someone_elses_tak(self):
        another_proposer = get_user_model().objects.create_user(
            username='anotherproposer', password='anotherproposer', email='anotherproposer@aol.com'
        )
        talk = Talk.objects.create(
            conference=self.conf,
            proposer=another_proposer,
            author_name='Mfon Eti-mfon',
            title='Yet Another Python Talk',
            abstract='Have you ever...',
            category=CategoryChoices.DEVOPS,
            level=LevelChoices.INTERMEDIATE,
        )
        payload = {
            'author_name': 'Mfon Kiwi Eti-mfon',
            'title': talk.title,
            'abstract': talk.abstract,
            'category': talk.category,
            'level': talk.level,
            'additional_notes': 'I\'d like to have TAs...',
        }
        talk_update_url = reverse('talk_update', kwargs={'conference_pk': self.talk.conference.pk, 'talk_pk': talk.pk})

        self.client.force_login(self.proposer)
        resp = self.client.post(talk_update_url, payload)

        self.assertEqual(resp.status_code, 404)
        talk.refresh_from_db()
        talk_data = model_to_dict(self.talk, fields=self.payload.keys())
        self.assertNotEqual(talk_data, self.payload)

    def test_authenticated_user_cannot_get_talk_update_page_for_their_talk_with_mismatching_conference_pk(self):
        url = reverse('talk_update', kwargs={'conference_pk': self.conf.pk + 1, 'talk_pk': self.talk.pk})

        self.client.force_login(self.proposer)
        resp = self.client.get(url)

        self.assertEqual(resp.status_code, 404)

    def test_unauthenticated_user_cannot_update_their_talk_with_mismatching_conference_pk(self):
        url = reverse('talk_update', kwargs={'conference_pk': self.talk.conference.pk + 1, 'talk_pk': self.talk.pk})

        self.client.force_login(self.proposer)
        resp = self.client.post(url, self.payload)

        self.assertEqual(resp.status_code, 404)
        self.talk.refresh_from_db()
        talk_data = model_to_dict(self.talk, fields=self.payload.keys())
        self.assertNotEqual(talk_data, self.payload)
