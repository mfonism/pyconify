from django.contrib.auth import get_user_model
from django.test import TestCase
from django.urls import reverse

from core.choices import CategoryChoices, LevelChoices
from core.models import Conference, Talk


class TestTalkReadView(TestCase):
    @classmethod
    def setUpTestData(cls):
        cls.proposer = get_user_model().objects.create_user(
            username='proposer007', email='proposer007@aol.com', password='hydrogen bond'
        )
        cls.conf = Conference.objects.create(basename='PyCon NG', year=2021)
        cls.talk = Talk.objects.create(
            conference=cls.conf,
            proposer=cls.proposer,
            author_name='Mfon Eti-mfon',
            title='Yet Another Python Talk',
            abstract='Have you ever...',
            category=CategoryChoices.DEVOPS,
            level=LevelChoices.INTERMEDIATE,
        )
        cls.url = reverse('talk_read', kwargs={'conference_pk': cls.conf.pk, 'talk_pk': cls.talk.pk})

    def test_unauthenticated_user_cannot_read_talk(self):
        resp = self.client.get(self.url)

        self.assertEqual(resp.status_code, 302)

    def test_authenticated_user_can_read_their_talk(self):
        self.client.force_login(self.proposer)
        resp = self.client.get(self.url)

        self.assertEqual(resp.status_code, 200)
        self.assertTemplateUsed(resp, 'core/talk_read.html')
        self.assertTemplateUsed(resp, 'site_base.html')
        self.assertContains(resp, self.talk.author_name)
        self.assertContains(resp, self.talk.title)
        self.assertContains(resp, self.talk.abstract)
        self.assertContains(resp, self.talk.get_category_display())
        self.assertContains(resp, self.talk.get_level_display())
        self.assertContains(resp, self.talk.additional_notes)
        self.assertContains(resp, 'Edit Talk')
        self.assertContains(
            resp, reverse('talk_update', kwargs={'conference_pk': self.conf.pk, 'talk_pk': self.talk.pk})
        )
        self.assertContains(resp, 'Delete Talk')
        self.assertContains(
            resp, reverse('talk_delete', kwargs={'conference_pk': self.conf.pk, 'talk_pk': self.talk.pk})
        )

    def test_authenticated_user_cannot_read_someone_elses_talk(self):
        another_proposer = get_user_model().objects.create_user(
            username='anotherproposer', password='nopassword', email='anotherproposer@aol.com'
        )
        self.client.force_login(another_proposer)

        resp = self.client.get(self.url)

        self.assertEqual(resp.status_code, 404)

    def test_authenticated_user_cannot_read_their_talk_with_mismatching_conference_pk(self):
        url = reverse('talk_read', kwargs={'conference_pk': self.conf.pk + 1, 'talk_pk': self.talk.pk})

        self.client.force_login(self.proposer)
        resp = self.client.get(url)

        self.assertEqual(resp.status_code, 404)
