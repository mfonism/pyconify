from django.contrib.auth import get_user_model
from django.forms.models import model_to_dict
from django.test import TestCase
from django.urls import reverse

from core.choices import CategoryChoices, LevelChoices
from core.models import Conference, Talk


class TestTalkCreateView(TestCase):
    @classmethod
    def setUpTestData(cls):
        cls.conf = Conference.objects.create(basename='PyCon NG', year=2021)
        cls.proposer = get_user_model().objects.create_user(
            username='proposer007', password='hydrogen bond', email='proposer007@aol.com'
        )
        cls.url = reverse('talk_create', kwargs={'conference_pk': cls.conf.year})
        cls.payload = {
            'author_name': 'Mfon Eti-mfon',
            'title': 'Yet Another Python Talk',
            'abstract': 'Have you ever...',
            'category': CategoryChoices.COMMUNITY_BUILDING,
            'level': LevelChoices.ALL,
            'additional_notes': '',
        }

    def test_authenticated_user_can_get_talks_proposal_page(self):
        self.client.force_login(self.proposer)

        resp = self.client.get(self.url)

        self.assertEqual(resp.status_code, 200)
        self.assertTemplateUsed(resp, 'core/talk_create.html')
        self.assertTemplateUsed(resp, 'site_base.html')
        self.assertContains(resp, 'author_name')
        self.assertContains(resp, 'title')
        self.assertContains(resp, 'abstract')
        self.assertContains(resp, 'category')
        self.assertContains(resp, 'level')
        self.assertContains(resp, 'additional_notes')
        self.assertContains(resp, 'Create Talk')

    def test_unauthenticated_user_cannot_get_talks_proposal_page(self):
        resp = self.client.get(self.url)

        self.assertTrue(resp.status_code, 302)

    def test_authenticated_user_can_propose_a_talk(self):
        talk_qs = Talk.objects.filter(conference=self.conf, proposer=self.proposer)
        old_talk_count = talk_qs.count()

        self.client.force_login(self.proposer)
        resp = self.client.post(self.url, self.payload)

        self.assertEqual(talk_qs.count(), old_talk_count + 1)
        created_talk = talk_qs.last()
        self.assertRedirects(
            resp, reverse('talk_read', kwargs={'conference_pk': self.conf.pk, 'talk_pk': created_talk.pk})
        )
        self.assertEqual(created_talk.proposer, self.proposer)
        self.assertEqual(created_talk.conference, self.conf)
        talk_data = model_to_dict(created_talk, fields=self.payload.keys())
        self.assertEqual(talk_data, self.payload)

    def test_unauthenticated_user_cannot_propose_a_talk(self):
        old_talk_count = Talk.objects.count()

        resp = self.client.post(self.url, self.payload)

        self.assertEqual(resp.status_code, 302)
        self.assertEqual(Talk.objects.count(), old_talk_count)
