from django.urls import include, path

from .views import (
    AboutView,
    ConductView,
    HomeView,
    SponsorsView,
    TalkCreateView,
    TalkDeleteSuccessView,
    TalkDeleteView,
    TalkReadView,
    TalkUpdateView,
)

urlpatterns = [
    path(
        '<int:conference_pk>/',
        include(
            [
                path('', HomeView.as_view(), name='home'),
                path('about/', AboutView.as_view(), name='about'),
                path('sponsors/', SponsorsView.as_view(), name='sponsors'),
                path('conduct/', ConductView.as_view(), name='conduct'),
                path('talks/proposal/', TalkCreateView.as_view(), name='talk_create'),
                path('talks/proposal/<int:talk_pk>/', TalkReadView.as_view(), name='talk_read'),
                path('talks/proposal/<int:talk_pk>/update', TalkUpdateView.as_view(), name='talk_update'),
                path('talks/proposal/<int:talk_pk>/delete', TalkDeleteView.as_view(), name='talk_delete'),
                path('talks/proposal/delete_success/', TalkDeleteSuccessView.as_view(), name='talk_delete_success'),
            ]
        ),
    )
]
